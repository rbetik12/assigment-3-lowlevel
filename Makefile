CC := gcc
CFLAGS := -ansi -pedantic-errors

CWARNINGS := all error
CFLAGS += $(addprefix -W,$(CWARNINGS))

all:
	$(CC) $(CFLAGS) main.c -o main.o



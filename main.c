#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

const int g_array_1[] = {1, 2, 3, 4, 5, 6, 7, 8};
const int g_array_2[] = {8, 9, 7, 4, 4, 3, 4, 4};

bool IsPrime(unsigned long const number) {
    size_t divisor;
    for (divisor = 1; divisor <= number; divisor++) {

        if (divisor == 1 || divisor == number) continue;

        if (number % divisor == 0) {
            return false;
        }
    }
    return true;
}

int ScalarProduct(int const * const array_1, int const * const array_2, size_t size) {
    int result = 0;
    size_t i;

    for (i = 0; i < size; i++) {
        result += array_1[i] * array_2[i];
    }

    return result;
}

int main(int argc, char** argv) {
    size_t size_1 = sizeof(g_array_1) / sizeof(g_array_1[0]);
    size_t size_2 = sizeof(g_array_2) / sizeof(g_array_2[0]);
    unsigned long input_number;

    if (size_1 != size_2) {
        fputs("error: Given vectors are not of the same size.", stderr);
        return EXIT_FAILURE;
    }

    printf("The result is %d\n", ScalarProduct(g_array_1, g_array_2, size_1));

    scanf("%lu", &input_number);

    printf(IsPrime(input_number) ? "Provided number is prime" : "Provided number is not prime");

    return 0;
}
